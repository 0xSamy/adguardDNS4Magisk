
#  Adguard DNS4Magisk
<h1 align="center">
<img src="https://i.ibb.co/gd2WQfM/Adguard-DNS4-Magisk.png">

Note*: This project is a fork of [CloudflareDNS4Magisk](https://forum.xda-developers.com/t/module-cloudflaredns4magisk.3772375/).

Adguard DNS4Magisk will override all default DNS values stored in getprop and add iptables rules at startup to forward all traffic to Adguard DNS servers.   

Unlike Couldflare which is known for its speed, Adguard is known for its good results in blocking ads. 

More information about Adguard: [https://adguard-dns.io/kb/general/dns-providers/](https://adguard-dns.io/kb/general/dns-providers/)

#### Warning:

Please note that your web provider can purely block all requests if you use customs DNS servers address. I can't do anything for that.

### Requirements:
- A rooted android device (something tells me if you're here it's because you have one..)
- Magisk installed (v17+ at least)

### Release

[adguardDNS4Magisk-1.1.zip](https://samy.link/projects/magisk/adguardDNS4Magisk/adguardDNS4Magisk-1.1.zip)
